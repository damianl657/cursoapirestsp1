package com.example.cursoApi.repositories;
import com.example.cursoApi.models.LibroModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface LibroRepository extends CrudRepository<LibroModel, Long> {
    
}
