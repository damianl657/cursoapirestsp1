package com.example.cursoApi.controllers;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.example.cursoApi.models.LibroModel;
import com.example.cursoApi.services.LibroService;

@RestController
@RequestMapping("/libros")
public class LibroController {
    @Autowired
    LibroService libService;
    @GetMapping(value = "/list")  
    public ArrayList<LibroModel> obtenerLibros(){
        return libService.obtenerLibros();
    }

    @PostMapping("/guardar")
    public LibroModel guardarLibro(@RequestBody LibroModel libro){
        return this.libService.guardarLibro(libro);
    }
    @GetMapping("/libro/{id}")
    public Optional<LibroModel> libXid(@PathVariable(value = "id") Long id ){
        System.out.println(id);
        return this.libService.getLibro(id);
    }

    @PutMapping("/update")
    public LibroModel updateLibro(@RequestBody LibroModel libro){
        return libService.updateLibro(libro);
    }
}
