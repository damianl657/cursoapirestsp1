package com.example.cursoApi.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.example.cursoApi.models.UsuarioModel;
import com.example.cursoApi.services.UsuarioService;



@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
	@Autowired
	UsuarioService usuarioServ;
	@GetMapping("/list")
	public ArrayList<UsuarioModel> obtenerUsuarios(){
		return usuarioServ.obtenerUsuarios();
	}
	
	@PostMapping("/guardar")
	public UsuarioModel guardarUsuario(@RequestBody UsuarioModel usuario) {
		return this.usuarioServ.guardarUsuario(usuario);
	}
	@GetMapping("/usuario/{id}")
	public Optional<UsuarioModel> usuXid(@PathVariable(value = "id") Long id){
		return this.usuarioServ.getUsuario(id);
	}
}
