package com.example.cursoApi.services;

import java.util.ArrayList;
import java.util.Optional;
import com.example.cursoApi.models.UsuarioModel;
import com.example.cursoApi.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
	@Autowired
	UsuarioRepository usRepository;
	
	public ArrayList<UsuarioModel> obtenerUsuarios(){
		return (ArrayList<UsuarioModel>) usRepository.findAll();
	}
	
	public UsuarioModel guardarUsuario(UsuarioModel usuario) {
		return usRepository.save(usuario);
	}
	public Optional<UsuarioModel> getUsuario(Long id){
		return usRepository.findById(id);
	}
}
