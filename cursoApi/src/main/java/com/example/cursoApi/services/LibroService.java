package com.example.cursoApi.services;

import java.util.ArrayList;
import java.util.Optional;

import com.example.cursoApi.models.LibroModel;
import com.example.cursoApi.repositories.LibroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LibroService {
    @Autowired
    LibroRepository libRepository;

    public ArrayList<LibroModel> obtenerLibros(){
        return (ArrayList<LibroModel>) libRepository.findAll();
    }

    public LibroModel guardarLibro(LibroModel libro){
        return libRepository.save(libro);
    }
    public Optional<LibroModel> getLibro(Long id){
        return libRepository.findById(id);

    }
    public LibroModel updateLibro(LibroModel libro){
        return libRepository.save(libro);
    }
}
